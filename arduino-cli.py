#!/usr/bin/env python3

import docker
import os
import sys
import argparse
import time

def get_build_container():
    client = docker.from_env()
    cwd = os.getcwd()

    image = client.images.get("target:latest")
    h = image.attrs['Id']

    for container in client.containers.list():
        if h == container.attrs['Image']:
            return container

    print("Container is not currently running. Please spin a new container.")
    #jdevices = ["/dev/ttyUSB0:/dev/ttyUSB0:rw"]
    #volumes = {cwd: {'bind': '/home/dev/projects', 'mode': 'rw'}}
    #container = client.containers.run(image, tty=True, detach=True, devices=devices, volumes=volumes)
    #return container

    return None


def main():
    parser = argparse.ArgumentParser(description='Execute build commands from within the ESP8266 docker container.')
    parser.add_argument('command', metavar='cmd', help='The comnmand to be run in the container.')
    args = parser.parse_args()

    cwd = os.getcwd()

    container = get_build_container()
    if not container: return

    client = docker.APIClient()
    src = client.inspect_container(container.id)['Mounts'][0]['Source']
    dest = client.inspect_container(container.id)['Mounts'][0]['Destination']

    try:
        suffix = cwd.split(src)[1]
    except:
        print("Working directory is not mounted in the Docker container.")
        return

    path = dest + suffix
    command = 'arduino-cli ' + args.command + ' ' + path
    container.exec_run(command)
    print(container.logs())


if __name__ == "__main__":
    main()
