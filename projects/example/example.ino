/*
ESP8266 Blink
Blink the blue LED on the ESP8266 module
*/

void setup() {
  pinMode(LED_BUILTIN, OUTPUT); // Initialize the LED pin as an output
}
// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, LOW); // Turn the LED on (Note that LOW is the voltage level)
  delay(1000); // Wait for a second
  digitalWrite(LED_BUILTIN, HIGH); // Turn the LED off by making the voltage HIGH
  delay(1000); // Wait for two seconds
}
