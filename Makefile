build:
	docker build -t target:latest .

run:
	docker run -it --device=/dev/ttyUSB0 -v $(shell pwd)/projects:/home/dev/projects target:latest
