#!/bin/bash

# This sets up the Arduino devleopment environment for the esp8266.

arduino-cli config init
install -T -o dev -g dev -m 0640 arduino-cli.yaml /home/dev/.arduino15
arduino-cli core update-index
arduino-cli core install arduino:avr
