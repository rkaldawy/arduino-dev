FROM archlinux:latest
MAINTAINER Remy Kaldawy (remykaldawy@gmail.com)

ENV BASE_DIR /root
ENV SUDOER wheel
ENV TERM xterm-256color

# Add target machine users.
RUN useradd dev -mG ${SUDOER}

WORKDIR ${BASE_DIR}

ADD machine /root/
RUN ./setup.sh

# Configure the Arduino IDE to use esp boards.
RUN install -T -o dev -g dev -m 0744 arduino_setup.sh /home/dev/arduino_setup.sh
RUN install -T -o dev -g dev -m 0644 arduino-cli.yaml /home/dev/arduino-cli.yaml
RUN su --command="/home/dev/arduino_setup.sh" - dev
RUN rm -f /home/dev/arduino_setup.sh /home/dev/arduino-cli.yaml

RUN ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key

# Set user account passwords
RUN chpasswd < ${BASE_DIR}/passwords

# Setup sudoers
RUN install -T -o 0 -g 0 -m 0440 sudoers /etc/sudoers

# Install pid 1
RUN install -T -o 0 -g 0 -m 0500 init /bin/init

#VOLUME ["/home/dev/programs"]

#CMD ["/usr/bin/bash"]
ENTRYPOINT /bin/init
